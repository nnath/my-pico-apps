#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.
#include "pico/multicore.h" // Required for using multiple cores on the RP2040.

/**
 * @brief LAB #02 - TEMPLATE
 *  author : NEHA NATH
 *        
 * 
 * @return int      Returns exit-status zero on completion.
 */

/**
 * @brief This function acts as the main entry-point for core #1.
 *        A function pointer is passed in via the FIFO with one
 *        incoming int32_t used as a parameter. The function will
 *        provide an int32_t return value by pushing it back on 
 *        the FIFO, which also indicates that the result is ready.
 */
void core1_entry() {
    while (1) {
        // 
        int32_t (*func)() = (int32_t(*)()) multicore_fifo_pop_blocking();
        int32_t p = multicore_fifo_pop_blocking();
        int32_t result = (*func)(p);
        multicore_fifo_push_blocking(result);
    }
}

  /**
 * @brief wallis_product_sp 
 * 
 * @param iteration specifies the number of iteration of wallis product function required to be computerd
 * @return single-precision float value which is the approximation of wallis product for iterations.
 */
float single_precision(int iterations){
    float result = 1;
    float i = 2;  // initialise top as 2 for the first two terms
    float n= 1;  // initialise bottom as 1 for the first term
    for(i; i <= iterations; i+=2){
        result = result * ( (i*i) / ( n* (n+ 2) ) );
        n+= 2;
    }
    result *= 2;
    return result;
}

/**
 * @brief wallis_product_sp 
 * 
 * @param iteration specifies the number of iteration of wallis product function required to be computed
 * @return double precision float value which is the approximation of wallis product for  iterations .
 */
double double_precision(int iterations){
    double result = 1;
    double i = 2;  // initialise top as 2 for the first two terms
    double n= 1;  // initialise bottom as 1 for the first term
    for(i; i <= iterations; i+=2){
        result = result * ( (i*i) / ( n* (n+ 2) ) );
        n+= 2;
    }
    result *= 2;
    return result;
}





bool get_xip_cache_en(){    // Get the flash memory address
    const uint8_t *flash_target_contents = (const uint8_t *) (XIP_CTRL_BASE); 
    if(flash_target_contents[0] == 1){    // Read the contents of memory address
        return true;
    }
    return false;
}

bool set_xip_cache_en(bool cache_en){ 
    //  Get the flash  memory  address      
    bool *a = (bool*) XIP_CTRL_BASE; 
    *a = cache_en; 
    return true;
}

bool flush_xip_cache_en(){
    // Get the flash  memory  flush  address
    bool *a = (bool*) XIP_CTRL_BASE + 0x04; 
    *a = 1; 
    return true;
}





//  Main entry point for the code.

int main() {

    const int    ITER_MAX  = 100000;

    stdio_init_all();
    //
    // Sequentially
    //

    printf("\n\n-----\nHello, multicore_runner!\n");

    set_xip_cache_en(0);
    bool xip_stat = get_xip_cache_en();
    printf("XIP Status: %d\n",xip_stat); 
    printf("Caches disabled:\n"); 



    // Code for sequential run 

    int single_startTime = time_us_32();                                  
    int single_seq       = time_us_32();

    single_precision(ITER_MAX);                             
    int single_seq_end = time_us_32();                                   //    The single-precision Wallis approximation
    int double_seq     = time_us_32();

    double_precision(ITER_MAX);                             
    int double_seq_end = time_us_32();                                    //    The double-precision Wallis approximation
    int single_endTime = time_us_32();    

    int Time_single    = single_endTime - single_startTime;
    int single_runtime = single_seq_end - single_seq;
    int double_runtime = double_seq_end - double_seq;

    printf("Time to run single-precision Wallis on single-core: %d\n",single_runtime); 
    printf("Time to run double-precision Wallis on single-core: %d\n",double_runtime); 
    printf("Time to run sequentially on single-core: %d\n",Time_single);                         //    Display time taken for application to run in sequential mode

    // launch the second core
    multicore_launch_core1(core1_entry);

    int parallel_startTime = time_us_32();                         
    int single_parallel = time_us_32();

    multicore_fifo_push_blocking((uintptr_t) &single_precision);
    multicore_fifo_push_blocking(ITER_MAX);                               //    The single-precision Wallis approximation on other core
    int double_parallel = time_us_32();

    double_precision(ITER_MAX);   
    int double_parallel_end = time_us_32();                                   //    The double-precision Wallis approximation on other core
    int res = multicore_fifo_pop_blocking();
    int single_parallel_end = time_us_32();
    int parallel_endTime = time_us_32();       

    int single_runtime_parallel = single_parallel_end - single_parallel;
    int double_runtime_parallel = double_parallel_end - double_parallel;
    int Time_parallel = parallel_endTime - parallel_startTime;

    printf("Time to run single-precision Wallis on both cores: %d\n",single_runtime_parallel); 
    printf("Time to run double-precision Wallis on both cores: %d\n",double_runtime_parallel); 
    printf("Time to run in parallel: %d\n",Time_parallel);                                           //    Display time taken for application to run in parallel mode
//-------//

    
    // Caches enabled
    set_xip_cache_en(1);
    xip_stat = get_xip_cache_en();
    printf("XIP Status: %d\n",xip_stat); 
    printf("Caches enabled:\n"); 

    single_startTime = time_us_32();                                  
    single_seq       = time_us_32();

    single_precision(ITER_MAX);                             
    single_seq_end = time_us_32();                                   //    The single-precision Wallis approximation
    double_seq     = time_us_32();

    double_precision(ITER_MAX);                             
    double_seq_end = time_us_32();                                    //    The double-precision Wallis approximation
    single_endTime = time_us_32();    

    Time_single    = single_endTime - single_startTime;
    single_runtime = single_seq_end - single_seq;
    double_runtime = double_seq_end - double_seq;

    printf("Time to run single-precision Wallis on single-core: %d\n",single_runtime); 
    printf("Time to run double-precision Wallis on single-core: %d\n",double_runtime); 
    printf("Time to run sequentially on single-core: %d\n",Time_single); 




    flush_xip_cache_en(); //Flush the cache
    multicore_reset_core1();

    multicore_launch_core1(core1_entry);

    parallel_startTime = time_us_32();                         
    single_parallel = time_us_32();

    multicore_fifo_push_blocking((uintptr_t) &single_precision);
    multicore_fifo_push_blocking(ITER_MAX);                               //    The single-precision Wallis approximation on other core
    double_parallel = time_us_32();

    double_precision(ITER_MAX);   
    double_parallel_end = time_us_32();                                   //    The double-precision Wallis approximation on other core
    res = multicore_fifo_pop_blocking();
    single_parallel_end = time_us_32();
    parallel_endTime = time_us_32();       

    single_runtime_parallel = single_parallel_end - single_parallel;
    double_runtime_parallel = double_parallel_end - double_parallel;
    Time_parallel = parallel_endTime - parallel_startTime;

    printf("Time to run single-precision Wallis on both cores: %d\n",single_runtime_parallel); 
    printf("Time to run double-precision Wallis on both cores: %d\n",double_runtime_parallel); 
    printf("Time to run in parallel: %d\n",Time_parallel);           



    return 0;
}
