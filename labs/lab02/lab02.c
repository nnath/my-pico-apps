#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#include "pico/stdlib.h"
//#include "pico/float.h"     // Required for using single-precision variables.
//#include "pico/double.h"    // Required for using double-precision variables.

#define WOKWI             // Uncomment if running on Wokwi RP2040 emulator.
#define ITERATION 100000    // Maximum number of iterations for the Wallis Product 
const double standard_value =3.14159265359; // Given value of pi 


/**
 * @brief LAB #02 - TEMPLATE
 *  author : NEHA NATH
 *        Main entry point for the code.
 * 
 * @return int      Returns exit-status zero on completion.
 */

float single_precision ()
{
     /**
 * @brief wallis_product_sp 
 * 
 * @param iteration specifies the number of iteration of wallis product function required to be computerd
 * @return single-precision float value which is the approximation of wallis product for iterations.
 */
    //div is the value = pi/2 
    float div=1;
    float n,i;
    for ( i=1; i<= ITERATION; i++)
    {
        n=(4*i*i);
        // Calculating value of div i.e pi/2 using Wallis product algorithm
        div = div * (n / (n-1) ) ;        
    }
    //Return the value of pi 
    return div *2.0 ;         

}

double double_precision ()
{
     /**
 * @brief wallis_product_sp 
 * 
 * @param iteration specifies the number of iteration of wallis product function required to be computed
 * @return double precision float value which is the approximation of wallis product for  iterations .
 */

   double div=1;
    double n,i;
    for ( i=1; i<= ITERATION; i++)
    {
        n=(4*i*i);
        // Calculating value of pi/2 using Wallis product 
        div = div * (n / (n-1) ) ;        
    }
    //Return the value of pi 
    return div *2.0 ;  
}

int main() {

#ifndef WOKWI
    // Initialise the IO as we will be using the UART
    // Only required for hardware and not needed for Wokwi
    stdio_init_all();
#endif

    // Print a console message to inform user what's going on.
    printf("Neha Nath - 21355247 \n");

    // Single Precision floationg point representation
    printf("Calculated value using single-precision (float) floating-point representation :  %f\n", single_precision());
    printf("Approximation Single-precision Error : %f\n", (1-(single_precision()/ standard_value)));

    // Double Precision floating point representation
    printf("Calculated value using double-precision (double) floating-point representation : %lf\n", double_precision());
    printf("Approximation double-precision Error : %lf\n", (1-(double_precision()/ standard_value)));

    // Returning zero indicates everything went okay.
    return 0;
}
